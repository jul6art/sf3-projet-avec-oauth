$(document).ready(function(){
	scrolll();
	menuResponsive();
    setTimeout(animePage, 310);
    setTimeout(animePageSuite, 560);
    animeLiens();
    mainfooter();
});

function scrolll(){
    $(".scrollbar").mCustomScrollbar({
		scrollButtons:{enable:true,scrollAmount: 125},
		keyboard:{scrollType:"stepped"},
		mouseWheel:{scrollAmount:125},
		theme:"rounded",//rounded-dots//rounded-dark//3d//dark-thin//minimal-dark
		autoExpandScrollbar:true,
		snapAmount:125,
		snapOffset:0
	});
}

function menuResponsive(){
	/* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('#nav-icon2').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with--volet');
		$(this).toggleClass('open');
    });
    
	/* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('#nav-icon3').click(function(e){
        e.preventDefault();
		setTimeout(function(){$('#nav-icon3').toggleClass('open');}, 1000);
        $('body').toggleClass('with--active--volet');
    });
    
	/* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('#nav-icon').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with--sidebar');
		$(this).toggleClass('open');
    });

    /* Je veux pouvoir masquer le menu si on clique sur le cache */
    $('#site-cache').click(function(e){
        $('body').removeClass('with--sidebar');
        $('#nav-icon1').removeClass('open');
        $('#nav-icon2').removeClass('open');
    })
}

function animePage(){
    $('.animation-loading').fadeOut(300);
}

function mainfooter(){
    $('.main-footer').click(function(){
        $('.main-footer').toggleClass('active');
    });
}

function animePageSuite(){
    $('.animation-loading').hide();
    $('body').removeClass('animated');
}

function animeLiens(){
    $('.link-animated .header a').click(function(e){
        a = $(this);
        if (a.attr('target') != '_blank'){
            e.preventDefault();
            $('.site-container').slideUp(300);
            setTimeout(function linkss(){
                window.location.pathname = a.attr('href');
            }, 300);
        }
    });
}