<?php

namespace APP\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('APPCoreBundle:Default:index.html.twig', array('full'=> 'full', 'menu'=> '3'));
    }
    public function fullAction()
    {
        return $this->render('APPCoreBundle:Default:index.html.twig', array('full'=> '', 'menu'=> ''));
    }
    public function menu2Action()
    {
        return $this->render('APPCoreBundle:Default:index.html.twig', array('full'=> 'full', 'menu'=> '2'));
    }
    public function menu3Action()
    {
        return $this->render('APPCoreBundle:Default:index.html.twig', array('full'=> '', 'menu'=> '3'));
    }
}
