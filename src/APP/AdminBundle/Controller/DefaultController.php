<?php

namespace APP\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('APPAdminBundle:Default:index.html.twig');
    }
}
