<?php

namespace APP\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Intl\Intl;

class UserType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $countries = Intl::getRegionBundle()->getCountryNames();
        $builder
            ->add('nom', 'text', array('attr'=>array('class'=>'form-control')))
            ->add('prenom', 'text', array('attr'=>array('class'=>'form-control')))
            ->add('dateNaissance', 'text', array('required'=>true,'attr'=>array('class'=>'form-control datepicker')))
            ->add('adresse', 'text', array('attr'=>array('class'=>'form-control')))
            ->add('codePostal', 'text', array('attr'=>array('class'=>'form-control')))
            ->add('localite', 'text', array('attr'=>array('class'=>'form-control')))
            ->add('Valider', 'submit', array('attr'=>array('class'=>'btn btn-success btn-form')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'APP\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_userbundle_user';
    }
}
