#base_project_sf3

A base empty symfony3 project with fosuser and OAuth integrated.

a question? geoffrey kratz <geoffreyk42@gmail.com>


## Auths ##

# /*********************************google************************************************/ #

## 1. create a google app: ##

https://console.developers.google.com

get key & secret

## 2. enter these keys in app/config/config.yml ##



```
#!yml

mlbo_auth:
    google:
        client_id:  ********************************
        client_secret:  ***************************************
        redirect_uri: http://domain.com/google/connect
        scope: https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile
```



# /***********************************facebook*********************************************/ #

## 1. create a facebook app ##

https://developers.facebook.com/

get key & secret, and enable project

## 2. enter these keys in app/config/config.yml ##


```
#!yml

mlbo_auth:
    facebook:
        client_id:  ********************************
        client_secret:  ***************************************
        redirect_uri: http://domain.com/facebook/connect/
        scope: public_profile, email
```